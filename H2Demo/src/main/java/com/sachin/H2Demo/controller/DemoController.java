package com.sachin.H2Demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sachin.H2Demo.entity.Student;
import com.sachin.H2Demo.service.StudentDao;

@Controller
public class DemoController {
	@Autowired
	public StudentDao stuDao;
	
	@RequestMapping("sHome")
	public String studentHome() {
		return "studentHome";
	}
	
	@RequestMapping("sSave")
	public String saveStudent(Student student) {
		
		stuDao.save(student);
		return "studentHome";
	}
	
	@RequestMapping("sDelete")
	public String deleteStudent(int id) {
		Optional<Student> stu =  stuDao.findById(id);
		Student student = stu.get();
		stuDao.deleteById(student.getId());
		return "studentHome";
	}
}
