package com.sachin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;



public class InstanceInnerClass {
	public static void main(String[] args) {
		
		ABC abc1 = new ABC() {
			@Override
			public void test() {
				System.out.println("Hello..");	
			}
		};
		
		abc1.test();
		
		ABC abc = ()->{
			System.out.println("Hello..");
		};
		
		abc.test();
		
		Runnable a = new Runnable() {
			@Override
			public void run() {
				System.out.println("anonymous runnable...");
			}
		};
		
		Runnable r = ()->{
			System.out.println("lambda runnable...");
		};
		
		Callable<Integer> c = new Callable<Integer>() {
			@Override
			public Integer call() throws Exception {
				System.out.println("anonymous callable...");
				return null;
			}
		};
		
		Callable<Integer> c1 = ()->{
			System.out.println("labmba runnable...");
			return 213;
		};
		
		DATA data = (String s)->{
			return 213;
		};
		
		ABCD abcd = (ab, bc, cd)->{
			return "";
		};
		
		A v = new A();
		A.B x = v.new B();
		x.test();
		
		/*A.B b = new A().new B();
		b.test();*/
		
	}
}

/*class A {
	class B{
		public void test() {
			System.out.println("Test.....");
		}
	}
}
*/

class A {
	class B{
		public void test() {
			System.out.println("Test.....");
		}
	}
}

class C {
	public void test2() {
		int a=99;
		class D {
			int data= 1;
			String name = "ASDF";
			public void test3() {
				System.out.println(a);
			}
		}
	}
}

interface ABC{
	public void test();
}

interface DATA{
	public int test(String s);
}

interface ABCD {
	public String test(int a, int b, int cd);
}