package com.sachin.SpringSecurity.repos;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sachin.SpringSecurity.support.GrantedAuthorityImpl;
import com.sachin.SpringSecurity.support.UserDetailsImpl;

@Repository
public interface UserDetailsRepo extends JpaRepository<UserDetailsImpl, Integer> {
	
	//@Query("select u from UserDetailsImpl u join u.authorities a where u.username=?1")
	public UserDetailsImpl findByUsername(String username);
	
	@Query("select a from UserDetailsImpl u join u.authorities a where u.username=?1")
	public Collection<GrantedAuthorityImpl> getByUsername(String username);
}
