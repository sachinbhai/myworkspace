package com.sachin.SpringSecurity.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.SpringSecurity.repos.UserDetailsRepo;
import com.sachin.SpringSecurity.support.GrantedAuthorityImpl;
import com.sachin.SpringSecurity.support.UserDetailsImpl;

@Controller
@RequestMapping("security")
public class MyController {
	
	@Autowired
	private UserDetailsRepo userDetailsRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@RequestMapping("/home")
	public ModelAndView showSecurityFeature() {
		/*String name = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println(name);
		List authorities = (List) SecurityContextHolder.getContext().getAuthentication().
				getAuthorities();
		System.out.println(authorities);*/
		
		return new ModelAndView("home");
	}
	
	@RequestMapping("/signUpPage")
	public ModelAndView signUpPage() {
		return new ModelAndView("registration");
	}
	
	@RequestMapping("/signUp")
	public ModelAndView signUp(UserDetailsImpl userDetailsImpl) {
		String password = userDetailsImpl.getPassword();
		password = passwordEncoder.encode(password);
		userDetailsImpl.setPassword(password);
		userDetailsImpl.setAccountNonExpired(true);
		userDetailsImpl.setAccountNonLocked(true);
		userDetailsImpl.setCredentialsNonExpired(true);
		userDetailsImpl.setEnabled(true);
		GrantedAuthorityImpl g1 = new GrantedAuthorityImpl();
		g1.setId(1);
		
		Collection<GrantedAuthorityImpl> collection =
				new ArrayList<GrantedAuthorityImpl>();
		collection.add(g1);
		userDetailsImpl.setAuthorities(collection);
		userDetailsRepo.save(userDetailsImpl);
		
		return new ModelAndView("loginHome");
	}
	
	@RequestMapping("/loginPage")
	public String loginPage() {
		return "loginHome";
	}
	
	@RequestMapping("/CM1")
	public String cm1() {
		return "CM1";
	}
	
	@RequestMapping("/CM2")
	public String cm2() {
		return "CM2";
	}
	
	@RequestMapping("/CM3")
	public String cm3() {
		return "CM3";
	}
	
	@RequestMapping("/CM4")
	public String cm4() {
		return "CM4";
	}
}

