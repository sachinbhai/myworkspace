package com.slokam.oauth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@RequestMapping("getData")
	public Map<Integer, String> getData(){
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(1,  "hello");
		map.put(2,  "how");
		map.put(3,  "are");
		map.put(4,  "you");
		return map;
	}
}
