package com.slokam.oauth.allService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import com.slokam.oauth.entities.ClientDetailsImpl;
import com.slokam.oauth.entities.GrantedAuthorityImpl;
@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		ClientDetailsImpl clientDetailsImpl = new ClientDetailsImpl();
		
		clientDetailsImpl.setAccessTokenValiditySeconds(160);
		clientDetailsImpl.setAdditionalInformation(null);
		
		GrantedAuthorityImpl grantedAuthorityImpl = new GrantedAuthorityImpl();
		grantedAuthorityImpl.setAuthority("CLIENT");
		
		Collection<GrantedAuthority> authoritise = new ArrayList();
		authoritise.add(grantedAuthorityImpl);
		clientDetailsImpl.setAuthorities(authoritise);
		
		Set<String> authorizedGrantTypes = new HashSet();
		authorizedGrantTypes.add("password");
		clientDetailsImpl.setAuthorizedGrantTypes(authorizedGrantTypes);
		clientDetailsImpl.setAutoApprove(true);
		clientDetailsImpl.setClientId(clientId);
		clientDetailsImpl.setClientSecret("abc");
		clientDetailsImpl.setRefreshTokenValiditySeconds(160);
		clientDetailsImpl.setRegisteredRedirectUri(null);
		clientDetailsImpl.setResourceIds(null);
		
		Set<String> scopes = new HashSet();
		scopes.add("resource-server-read");
		scopes.add("resource-server-write");
		clientDetailsImpl.setScope(scopes);
		
		clientDetailsImpl.setScoped(true);
		clientDetailsImpl.setSecretRequired(true);
		
		return clientDetailsImpl;
	}

}
