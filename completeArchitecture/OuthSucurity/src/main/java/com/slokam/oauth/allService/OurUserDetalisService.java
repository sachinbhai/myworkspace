package com.slokam.oauth.allService;

import java.util.ArrayList;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.slokam.oauth.allDaos.UserRepository;
import com.slokam.oauth.entities.OurUserDetails;
import com.slokam.oauth.entities.GrantedAuthorityImpl;



@Component
public class OurUserDetalisService implements UserDetailsService{
	@SuppressWarnings("unused")
	@Autowired
	private OurUserDetails userDetails;
	@Autowired
	private UserRepository userRepository;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		//userDetails.setUsername(username);
		//userDetails.setPassword("1user");
		//userDetails.setAccountNonExpired(true);
		//userDetails.setAccountNonLocked(true);
		//userDetails.setCredentialsNonExpired(true);
		//userDetails.setEnabled(true);
		
		//UserGrantedAuthority authority = new UserGrantedAuthority();
		//UserGrantedAuthority authority1 = new UserGrantedAuthority();
		
		//authority.setAuthority("ROLE_user");
		//authority1.setAuthority("ROLE_admin");
		
		//Collection<UserGrantedAuthority> userGrantedAuthority = new ArrayList<UserGrantedAuthority>();
		//userGrantedAuthority.add(authority);
		//userGrantedAuthority.add(authority1);
		
		//userDetails.setAuthorities(userGrantedAuthority);
		
		//userRepository.save(userDetails);
		//===============================================================================================
		
		OurUserDetails userDetails = userRepository.findByUsername(username);
		System.out.println(userDetails.getId()+"=="+userDetails.getUsername()+"=="+userDetails.getPassword()+"=="+userDetails.isAccountNonExpired()+"=="+userDetails.isAccountNonLocked()+"=="+userDetails.isCredentialsNonExpired()+"=="+userDetails.isEnabled());
		Collection<GrantedAuthorityImpl> authorities = userRepository.findByAuthorities(username);
		for (GrantedAuthorityImpl userGrantedAuthority : authorities) {
			System.out.println(userGrantedAuthority.getAuthority());
		}
		System.out.println(authorities.containsAll(authorities));
		userDetails.setAuthorities(authorities);
		
		return userDetails;
	}
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new PasswordEncoder() {

			@Override
			public String encode(CharSequence rawPassword) {

				return rawPassword.toString();
			}

			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {

				return rawPassword.toString().equals(encodedPassword);
			}
			
		};
		
	}
	
	/*
	 * @Bean public PasswordEncoder passwordEncoder() { return new
	 * BCryptPasswordEncoder();
	 * 
	 * 
	 * }
	 */


}
