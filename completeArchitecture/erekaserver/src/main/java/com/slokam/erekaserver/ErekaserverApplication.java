package com.slokam.erekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ErekaserverApplication {

		public static void main(String[] args) {
			SpringApplication.run(ErekaserverApplication.class, args);
		}
	
}
