package com.sachin.RestApp1;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RestApp1Controller {
	
	@GetMapping("/restApp1Test")
	public String runApp2(){
		return "restApp1Test";
	}
}
