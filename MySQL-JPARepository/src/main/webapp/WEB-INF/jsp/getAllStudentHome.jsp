<%@page import="com.sachin.MySQLJPARepository.entity.Student"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
	function sSave() {
		var name = document.forms[0].name.value;
		var age = document.forms[0].age.value;
		var qual = document.forms[0].qual.value;
		var marks = document.forms[0].marks.value;
		
		if(trim(name)==""|trim(age)==""|trim(qual)==""|trim(marks)==""){
			alert("Please Enter Data");
			return;
		}
		document.forms[0].action="sSave";
		document.forms[0].submit();
	}
	function sDelete() {
		document.forms[0].action = "sDelete";
		document.forms[0].submit();
	}
	function sUpdate() {
		document.forms[0].action = "sUpdate";
		document.forms[0].submit();
	}
	function sGet() {
		document.forms[0].action = "sGet";
		document.forms[0].submit();
	}
	function sGetAll() {
		document.forms[0].action = "sGetAll";
		document.forms[0].submit();
	}
</script>
</head>

<body>
	<form>
		<fieldset>
			<!-- Student ID: -->
			<input type="hidden" name="id"><br> 
			Student Name: <input
				type="text" name="name"><br> 
			Student Age: <input
				type="number" name="age"><br> 
			Student Qual: <input
				type="text" name="qual"><br> 
			Student Marks:  <input type="number" name="marks"><br>
			Passport No:  <input type="text" name="pptObj.passportNo"><br>
			Passport IssueDate:  <input type="date" name="pptObj.issueDate"><br>
			Passport ExpDate:  <input type="date" name="pptObj.expDate"><br><br> 
			
			<c:if test="${student.id==null }">
			
				<input type="button" value="SAVE"  onclick="sSave()">
				<input type="button" value="DEL" disabled="disabled" onclick="sDelete()">
				<input type="button" value="UPDATE" disabled="disabled" onclick="sUpdate()">
				
			</c:if>
			
			<c:if test="${student.id!=null }">
			
				<input type="button" value="SAVE" disabled="disabled"  onclick="sSave()">
				<input type="button" value="DEL" onclick="sDelete()">
				<input type="button" value="UPDATE" onclick="sUpdate()">
				
			</c:if>
			
			<input type="button" value="GETALL" onclick="sGetAll()">

		</fieldset>
	</form>

	<%
		List<Student> stuList = (List<Student>) request.getAttribute("listObj");
		if (stuList.size() > 0) {
	%>
	<table>
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>AGE</th>
			<th>QUALIFICATION</th>
			<th>MARKS</th>
			<th>PASSPORT #</th>
			<th>ISSUE-DATE</th>
			<th>EXP-DATE</th>
		</tr>
		
		
		<c:forEach var="s" items="${listObj}" >
	
		<tr>
			<td><font color="red"><a href = "sGet?id=${s.id}">${s.id}</a></font></td>
			<td><font color="red">${s.name}</font></td>
			<td><font color="red">${s.age}</font></td>
			<td><font color="red">${s.qual}</font></td>
			<td><font color="red">${s.marks}</font></td>
			<td><font color="red">${s.pptObj.passportNo}</font></td>
			<td><font color="red">${s.pptObj.issueDate}</font></td>
			<td><font color="red">${s.pptObj.expDate}</font></td>
		</tr>
		</c:forEach>

	</table>

	<%
		} else {
	%>
	<h3>No Records found</h3>
	<%
		}
	%>

</body>
</html>

