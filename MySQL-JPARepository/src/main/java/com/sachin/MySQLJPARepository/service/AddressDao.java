package com.sachin.MySQLJPARepository.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.MySQLJPARepository.entity.Address;

@Repository
public interface AddressDao extends JpaRepository<Address, Integer> {

}
