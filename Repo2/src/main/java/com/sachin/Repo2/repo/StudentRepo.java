package com.sachin.Repo2.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sachin.Repo2.entity.Student;
import com.sachin.Repo2.entity.Passport;

@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {
	
	@Query("select s.name from Student s join s.passport p where p.passportNo=?1")
	public abstract String getStudentNameByPassportNo(String passportNo);
	
	@Query("select s.name from Student s join s.passport p where p.passportNo like ?1")
	public abstract List<String> getStudentNamesByPassportNoLike(String passportNo);

}
