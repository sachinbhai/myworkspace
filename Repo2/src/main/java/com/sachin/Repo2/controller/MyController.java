package com.sachin.Repo2.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.Repo2.entity.Student;
import com.sachin.Repo2.repo.StudentRepo;

@Controller
public class MyController {
	
	@Autowired
	public StudentRepo sRepo;
	
	@RequestMapping("searchPage")
	public String searchByPassportNo() {
		return "search";
	}

	@RequestMapping("searchResult")
	public ModelAndView getName(String passportNo) {
		
		String name = sRepo.getStudentNameByPassportNo(passportNo); 
		ModelAndView mv = new ModelAndView("outputStudentName", "stuName", name);
		return mv;
	}
	
	@RequestMapping("searchResultNames")
	public ModelAndView getLikeNames(String passportNo) {
		String x = passportNo+"%";
		
		List<String> namesList = sRepo.getStudentNamesByPassportNoLike(x);
		System.out.println(namesList.size());
		ModelAndView mv = new ModelAndView("outputLikeStudents", "stuNames", namesList);
		return mv;
	}
	
	@RequestMapping("sHome")
	public String studentHome() {
		return "homeInput";
	}
	
	@RequestMapping("sSave")
	public String saveStudent(Student student) {
		
		student.getPassport().setStudent(student);;
		sRepo.save(student);
		return "homeInput";
	}
	
	@RequestMapping("sGet")
	public ModelAndView getStudent(int id) {
		Optional<Student> stu =  sRepo.findById(id);
		Student student = stu.get();
		student.getPassport().setStudent(student);
		ModelAndView mv = new ModelAndView("homeInput", "studentObj", student);
		return mv;
	}
	
	@RequestMapping("sGetAll")
	public ModelAndView getAllStudents() {
		List<Student> stuList =  sRepo.findAll();
		ModelAndView mv = new ModelAndView("getAllStudents", "listObj", stuList);
		return mv;
	}
	
	/*@RequestMapping("sDelete")
	public String deleteStudent(int id) {
		Optional<Student> stu =  sRepo.findById(id);
		Student student = stu.get();
		sRepo.deleteById(student.getId());
		return "homeInput";
	}
	
	@RequestMapping("sUpdate")
	public String updateStudent(Student student) {
		student.getPassport().setStudent(student);
		sRepo.save(student);
		return "homeInput";
	}*/
}
