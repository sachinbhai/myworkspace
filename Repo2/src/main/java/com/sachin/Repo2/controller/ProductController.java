package com.sachin.Repo2.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


import com.sachin.Repo2.entity.Product;
import com.sachin.Repo2.exceptions.ServerSideValidationException;
import com.sachin.Repo2.repo.ProductRepo;
import com.sachin.Repo2.service.ProductService;

@Controller
@RequestMapping("product")
public class ProductController {
	
	@Autowired
	public ProductService pService;
	
	@RequestMapping("/home")
	public ModelAndView productHome() {
		return new ModelAndView("formHome", "productAtt", new Product());
	}
	
	@RequestMapping("/save")
	public ModelAndView saveProduct(@ModelAttribute("productAtt") @Valid Product product,BindingResult br) throws ServerSideValidationException {
				
		if(!br.hasErrors()) {
			pService.imgValid(product);
			pService.moreThanAvail(product);
			pService.saveProduct(product);
		}else {
			
			return new ModelAndView("formHome", "productAtt", product);
		}
		pService.imgValid(product);
		return new ModelAndView("formHome", "productAtt", new Product());
	}
	
	
	/*@RequestMapping("/getAllPage")
	public ModelAndView getAllProducts() {
		List<Product> proList = pRepo.findAll();
		return new ModelAndView("formHome", "proListAtt", proList);
	}
	
	@RequestMapping("/getOne")
	public ModelAndView getStudent(int id) {
		Optional<Product> pro =  pRepo.findById(id);
		Product proObj = pro.get();
		ModelAndView mv = new ModelAndView("forHome", "proObj", proObj);
		return mv;
	}*/
	
}
