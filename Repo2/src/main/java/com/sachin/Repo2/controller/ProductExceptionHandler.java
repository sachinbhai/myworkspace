package com.sachin.Repo2.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.Repo2.exceptions.ServerSideValidationException;

@ControllerAdvice
public class ProductExceptionHandler {
	
	@ExceptionHandler(ServerSideValidationException.class)
	public ModelAndView someMethod(ServerSideValidationException e) {
		ModelAndView mv = new ModelAndView("error", "messageAtt", e.getMessage());
		return mv;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView someOtherMethod(Exception ex) {
		ModelAndView mv = new ModelAndView("error", "messageAtt", ex.getMessage());
		return mv;
	}
	
	@ExceptionHandler(IOException.class)
	public ModelAndView anotherMethod(IOException ioe) {
		ModelAndView mv = new ModelAndView("error", "messageAtt", ioe.getMessage());
		ioe.printStackTrace();
		return mv;
	}
	
}
