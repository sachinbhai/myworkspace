package com.sachin.Repo2.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.Repo2.entity.Product;

@Controller
public class UploadController {
	
	private static final Logger log = LoggerFactory.getLogger(UploadController.class);
	
	@Value("${file.uploadLocation}")
	public String folderName;

	List<Product> productList = new ArrayList<>();

	@RequestMapping("/excelUpload")
	public ModelAndView uploadFile(MultipartFile uploadedFile) throws IOException {
		log.info("Entered uploadFile method");
		String fileName = uploadedFile.getOriginalFilename();

		if ((fileName.endsWith("xls")) || (fileName.endsWith("xlsx"))) {
			log.info("uploaded file is of valid extention");
			File file = new File(folderName + fileName);
			uploadedFile.transferTo(file);
			log.info("file saved in server");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheetAt(0);
			int lastRowNumber = sheet.getLastRowNum();
			log.info("row retriver process going to start");
			for (int i = 0; i <= lastRowNumber; i++) {
				
				Product product = new Product();
				DataFormatter formatter = new DataFormatter();
				XSSFRow row = sheet.getRow(i);
				XSSFCell cell0 = row.getCell(0);
				String name = formatter.formatCellValue(cell0);
				XSSFCell cell1 = row.getCell(1);
				Integer price = Integer.parseInt(formatter.formatCellValue(cell1));
				XSSFCell cell2 = row.getCell(2);
				String color = formatter.formatCellValue(cell2);
				XSSFCell cell3 = row.getCell(3);
				Integer avail = Integer.parseInt(formatter.formatCellValue(cell3));
				XSSFCell cell4 = row.getCell(4);
				String panCard = formatter.formatCellValue(cell4);

				product.setName(name);
				product.setAvail(avail);
				product.setColor(color);
				product.setPanCard(panCard);
				product.setPrice(price);

				productList.add(product);
			}
			log.info("objects added into productList");
		} else {
			throw new IOException();
		}
		return new ModelAndView("formHome", "productAtt", new Product());
	}

}
