package com.sachin.BankAndAccounts.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.BankAndAccounts.entity.Account;
import com.sachin.BankAndAccounts.entity.Branch;
import com.sachin.BankAndAccounts.repos.AccountRepo;

@RestController
@RequestMapping("account")
public class AccountController {
	
	@Autowired
	public AccountRepo acRepo;
	
	@PostMapping("/saveOne")
	public ResponseEntity saveAccount(@RequestBody Account account) {
		acRepo.save(account);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity getBranchById(@PathVariable Integer id) {
		Optional<Account> account = acRepo.findById(id);
		account.get().setBranch(null);
		return new ResponseEntity<>(account.get(), HttpStatus.OK);
	}
}
