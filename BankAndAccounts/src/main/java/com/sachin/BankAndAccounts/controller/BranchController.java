package com.sachin.BankAndAccounts.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.BankAndAccounts.entity.Account;
import com.sachin.BankAndAccounts.entity.Branch;
import com.sachin.BankAndAccounts.repos.AccountRepo;
import com.sachin.BankAndAccounts.repos.BranchRepo;

@RestController
@RequestMapping("branch")
public class BranchController {
	
	@Autowired
	public BranchRepo brRepo;
	
	@PostMapping("/saveOne")
	public ResponseEntity saveBranch(@RequestBody Branch branch) {
		List<Account>accountList = branch.getAccountList();
		for (Account account : accountList) {
			account.setBranch(branch);
		}
		brRepo.save(branch);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity getBranchById(@PathVariable Integer id) {
		Optional<Branch> branch = brRepo.findById(id);
		List<Account> accountList = branch.get().getAccountList();
		for (Account account : accountList) {
			account.setBranch(null);
		}
		return new ResponseEntity<>(branch.get(), HttpStatus.OK);
	}
	
	

}
