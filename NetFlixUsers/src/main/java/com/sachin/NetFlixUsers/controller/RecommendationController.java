package com.sachin.NetFlixUsers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.NetFlixUsers.entity.Video;
import com.sachin.NetFlixUsers.repos.VideoRepo;
import com.sachin.NetFlixUsers.service.RecommendationService;

@RestController
@RequestMapping("recommendations")
public class RecommendationController {
	
	@Autowired
	public RecommendationService rService;
	
	@GetMapping("/byGenre/{id}")
	public ResponseEntity<List<Video>> getVideosByGenre(@PathVariable Integer id){
		List<Video> videoList = rService.getVideosByGenre(id);
		return new ResponseEntity<List<Video>>(videoList, HttpStatus.OK);
	}
	
	@GetMapping("/byCountry/{id}")
	public ResponseEntity<List<Video>> getVideosByCountry(@PathVariable Integer id){
		List<Video> videoList = rService.getVideosByCountryByUser(id);
		return new ResponseEntity<List<Video>>(videoList, HttpStatus.OK);
	}
	
	
}
