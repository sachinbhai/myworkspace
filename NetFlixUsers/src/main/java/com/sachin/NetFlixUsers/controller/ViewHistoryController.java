package com.sachin.NetFlixUsers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.NetFlixUsers.entity.UserViewHistory;
import com.sachin.NetFlixUsers.repos.ViewHistoryRepo;

@RestController
@RequestMapping("viewHistory")
public class ViewHistoryController {
	@Autowired
	private ViewHistoryRepo viewRepo;
	
	@PostMapping("/saveViewHistory")
	public ResponseEntity<UserViewHistory> saveHistory(@RequestBody UserViewHistory viewHistory){
		viewRepo.save(viewHistory);
		return new ResponseEntity<>(viewHistory, HttpStatus.CREATED);
	}
}
