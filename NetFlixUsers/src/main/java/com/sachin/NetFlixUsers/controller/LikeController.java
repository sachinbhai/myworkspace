package com.sachin.NetFlixUsers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.NetFlixUsers.entity.VideoLikes;
import com.sachin.NetFlixUsers.repos.LikeRepo;

@RestController
@RequestMapping("videoLike")
public class LikeController {

	@Autowired
	private LikeRepo likeRepo;
	
	@PostMapping("/saveLike")
	public ResponseEntity<VideoLikes> saveLike(@RequestBody VideoLikes vLike){
		likeRepo.save(vLike);
		return new ResponseEntity<>(vLike, HttpStatus.CREATED);
	}
}
