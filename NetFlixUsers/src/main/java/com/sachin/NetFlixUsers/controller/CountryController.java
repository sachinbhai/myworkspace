package com.sachin.NetFlixUsers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.NetFlixUsers.entity.Country;
import com.sachin.NetFlixUsers.repos.CountryRepo;

@RestController
@RequestMapping("country")
public class CountryController {
	@Autowired
	private CountryRepo cRepo;
	
	@PostMapping("/saveCountry")
	public ResponseEntity<Country> saveCountry(@RequestBody Country country){
		cRepo.save(country);
		return new ResponseEntity<>(country, HttpStatus.CREATED);
	}
}
