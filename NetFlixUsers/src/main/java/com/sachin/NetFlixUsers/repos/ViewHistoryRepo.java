package com.sachin.NetFlixUsers.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.NetFlixUsers.entity.UserViewHistory;

@Repository
public interface ViewHistoryRepo extends JpaRepository<UserViewHistory, Integer> {

}
