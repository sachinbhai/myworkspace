package com.sachin.NetFlixUsers.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.NetFlixUsers.entity.Genre;

@Repository
public interface GenreRepo extends JpaRepository<Genre, Integer> {

}
