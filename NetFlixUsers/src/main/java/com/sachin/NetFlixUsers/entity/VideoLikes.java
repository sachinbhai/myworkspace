package com.sachin.NetFlixUsers.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
//LikeController
public class VideoLikes {
	
	@Id
	@GeneratedValue
	private Integer id;
	@DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date time;
	@ManyToOne()
	@JoinColumn(name="vid")
	private Video likedVideo;
	@ManyToOne()
	@JoinColumn(name="uid")
	private User user;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Video getLikedVideo() {
		return likedVideo;
	}
	public void setLikedVideo(Video likedVideo) {
		this.likedVideo = likedVideo;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
