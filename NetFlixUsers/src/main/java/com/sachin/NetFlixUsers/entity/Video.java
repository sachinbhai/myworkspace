package com.sachin.NetFlixUsers.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Video { 

	/*@DateTimeFormat(pattern="hh-mm-ss")
	private Date videoLength;*/
	
	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	@ManyToOne()
	@JoinColumn(name="gnid")
	private Genre videoGenre;
	@ManyToOne()
	@JoinColumn(name="cid")
	private Country videoCountry;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Genre getVideoGenre() {
		return videoGenre;
	}
	public void setVideoGenre(Genre videoGenre) {
		this.videoGenre = videoGenre;
	}
	public Country getVideoCountry() {
		return videoCountry;
	}
	public void setVideoCountry(Country videoCountry) {
		this.videoCountry = videoCountry;
	}
	
}
