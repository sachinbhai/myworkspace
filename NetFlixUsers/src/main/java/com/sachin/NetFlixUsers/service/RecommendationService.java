package com.sachin.NetFlixUsers.service;

import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sachin.NetFlixUsers.entity.Video;
import com.sachin.NetFlixUsers.repos.VideoRepo;

@Service
public class RecommendationService {
	
	@Autowired
	public VideoRepo vRepo;
	
	//by native query
	/*public List<Video> getVideosByGenreByUser(Integer id){
		List<Integer> idvs = vRepo.getVideosByGenreByUserId(id);
		List<Video> videoList = vRepo.findAllById(idvs);
		return videoList;
	}*/
	
	public List<Video> getVideosByGenre(Integer id){
		List<Video> videoList = vRepo.getVideosByGenreByUserId(id);
		List<Video> videoListWatched = getVideosWatchedByUser(id);
		List<Video> videoList1 = ListUtils.subtract(videoList, videoListWatched);
		return videoList1;
	}
	
	public List<Video> getVideosByCountryByUser(Integer id){
		List<Video> videoList = vRepo.getVideosByCountryByUserId(id);
		return videoList;
	}
	
	public List<Video> getVideosWatchedByUser(Integer id){
		List<Video> videoList = vRepo.getVideosWatchedByUser(id);
		return videoList;
	}
}
