package com.sachin.deadLock;

public class Thread1 extends Thread{
		
		private ObjectOne obj1;
	
		public Thread1(ObjectOne obj1) {
			this.obj1 = obj1;
		}

		@Override
		public void run() {
			obj1.methodOne();
		}
}
