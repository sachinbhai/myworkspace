package com.sachin.deadLock;

public class Thread2 extends Thread{
	
	private ObjectTwo obj2;
	
	public Thread2(ObjectTwo obj2) {
		this.obj2 = obj2;
	}

	@Override
	public void run() {
		obj2.methodTwo();
	}
}
