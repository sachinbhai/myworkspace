package com.sachin.deadLock;

public class DeadLockTest {
	public static void main(String[] args) {
		ObjectOne obj1 = new ObjectOne();
		ObjectTwo obj2 = new ObjectTwo();
		
		obj1.setObj2(obj2);
		obj2.setObj1(obj1);
		
		Thread th1 = new Thread1(obj1);
		Thread th2 = new Thread2(obj2);
		
		th1.start();
		th2.start();
		
	}

}
