package com.sachin.deadLock;

public class ObjectTwo{
	
	private ObjectOne obj1;
	
	public void setObj1(ObjectOne obj1) {
		this.obj1 = obj1;
	}


	public synchronized void methodTwo() {
		System.out.println(Thread.currentThread().getName()+" : locked resource 2");
		try {
			Thread.currentThread().sleep(1000);
			obj1.methodOne();
		} catch (InterruptedException e) {
			System.out.println("bhasker");
			e.printStackTrace();
		}
		
	}

}
