package com.sachin.deadLock;

public class ObjectOne{
		
	private ObjectTwo obj2;
	
	public void setObj2(ObjectTwo obj2) {
		this.obj2 = obj2;
	}

	public synchronized void methodOne() {
		 System.out.println(Thread.currentThread().getName()+" : locked resource 1"); 
		try {
			Thread.currentThread().sleep(1000);
			obj2.methodTwo();
		} catch (InterruptedException e) {
			System.out.println("harish");
			e.printStackTrace();
		}
		
	}

}
