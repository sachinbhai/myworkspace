package com.sachin;

import java.util.Set;

public class Thread1 extends Thread {
	@Override
	public void run() {
		if(Thread.currentThread().getName().equals("one")) {
			Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
			for (Thread thread : threadSet) {
				System.out.println(thread.getName());
			}
			try {
				Thread.currentThread().join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(int i=1; i<=100; i++) {
			System.out.println(Thread.currentThread().getName()+" :"+i);
		}
	}
}
