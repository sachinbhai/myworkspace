package com.sachin.arrayBlockingQueue;

import java.util.concurrent.ArrayBlockingQueue;

public class ArrayBlockingQueueTest {
	
	public static void main(String[] args) {
		
		ArrayBlockingQueue<String> abq = new ArrayBlockingQueue<>(1);
		
		Thread pth = new ABQProducer(abq);
		pth.start();
		Thread cth = new ABQConsumer(abq);
		cth.start();
		
	}
}
