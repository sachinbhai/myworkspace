package com.sachin.arrayBlockingQueue;

import java.util.concurrent.ArrayBlockingQueue;

public class ABQConsumer extends Thread {
	
	private ArrayBlockingQueue<String> abq;
	
	public ABQConsumer(ArrayBlockingQueue<String> abq) {
		this.abq = abq;
	}

	@Override
	public void run() {
		for(int i=1; i<=100; i++) {
			try {
				System.out.println("Consumer Thread::"+abq.take());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
