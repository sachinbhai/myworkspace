package com.sachin.arrayBlockingQueue;

import java.util.concurrent.ArrayBlockingQueue;

public class ABQProducer extends Thread{
	
	private ArrayBlockingQueue<String> abq;
	
	public ABQProducer(ArrayBlockingQueue<String> abq) {
		this.abq = abq;
	}

	@Override
	public void run() {
		for(int i=1; i<=100; i++) {
			try {
				abq.put(i+"");
				System.out.println("Producer: "+i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
