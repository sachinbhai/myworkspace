package com.sachin.liveLockExample;

public class LiveLockTest {
	public static void main(String[] args) {
		final Worker worker1 = new Worker("worker 1", true);
		final Worker worker2 = new Worker("worker 2", true);
		
		final CommonResource s = new CommonResource(worker1);
		
		new Thread(() -> {
            worker1.work(s, worker2);
        }).start();

        new Thread(() -> {
            worker2.work(s, worker1);
        }).start();
        
	}
}
