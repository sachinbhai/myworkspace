package com.sachin.liveLockExample;

public class Worker {
	private String name;
	private boolean active;
	
	public Worker(String name, boolean active) {
		this.name = name;
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public boolean isActive() {
		return active;
	}

	public synchronized void work(CommonResource commonResource, Worker otherWorker) {
		
		while(active) {
			//wait for resource to become available
			if(commonResource.getOwner() != this) {
				try {
					wait(10);
				}catch(InterruptedException e) {
					//ignore
				}
				continue;
			}
		
		
			//if the other worker is also active, let it do the work first
			if(otherWorker.isActive()) {
				
				System.out.println(getName()+" :handover the "
						+ "resource to the worker: "+ otherWorker.getName());
				commonResource.setOwner(otherWorker);
				continue;
			}
			
			//now use the commonResourse
			System.out.println(getName()+": working on the commonResource");
			active = false;
			commonResource.setOwner(otherWorker);
		}
	}
	

}