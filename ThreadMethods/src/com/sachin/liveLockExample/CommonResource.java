package com.sachin.liveLockExample;

public class CommonResource {
	
	private Worker owner;

	public CommonResource(Worker d) {
		this.owner = d;
	}

	public Worker getOwner() {
		return owner;
	}

	public void setOwner(Worker owner) {
		this.owner = owner;
	}
	
}
