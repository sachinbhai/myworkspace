package com.sachin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("one");
		list.add("two");
		list.add("three");
		list.add("four");
		
		System.out.println("+++++++++++++++++++++1+++");
		
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		System.out.println("+++++++++++++++++++++2+++");
		
		for (String string : list) {
			System.out.println(string);
		}
		
		System.out.println("+++++++++++++++++++++3+++");
		
		Iterator<String> s = list.iterator();
		
		while(s.hasNext()) {
			System.out.println(s.next());
		}
		
		System.out.println("+++++++++++++++++++++4+++");
		
		Object[] obj = list.toArray();
		for (Object object : obj) {
			System.out.println(object);
		}
			
		System.out.println("+++++++++++++++++++++5+++");
		
		String[] strArr= new String[list.size()];
		strArr = list.toArray(strArr);
		for(int i=0; i<strArr.length; i++) {
			System.out.println(strArr[i]);
		}
		
		System.out.println("+++++++++++++++++++++6+++");
		
		list.forEach((String)->{
			System.out.println(String);
		});
		
		System.out.println("+++++++++++++++++++++7+++");
		
/*		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				System.out.println("Anonymous Inner Class");
			}
		};
		
		Thread t = new Thread(runnable);
		t.start();
*/		
		System.out.println("+++++++++++++++++++++8+++");
		
		Random random = new Random();
		
		System.out.println(random.ints(1, 20).limit(10).map((a)->{
			return a*a;
		}).max()); // not getting the right answer
		
		OptionalInt opt = random.ints(1, 20).limit(10).map((a)->{
			return a*a;
		}).max();
		
		System.out.println(opt.getAsInt()); // not getting the right answer
		
		IntStream stream = random.ints(1, 20).limit(10).map((a)->{
			return a*a;
		});
		
		stream.forEach(System.out::println);
		
		System.out.println("+++++++++++++++++++++9+++");
		
		IntStream stream1 = random.ints(1, 20).limit(10);
		
		stream1.filter((v)->{
			int count = 0;
			for (int i=2; i<v; i++) {
				if(v%i==0) 
				count++;
			}
			if(count>0) return false;
			else return true;
		}).distinct().forEach(System.out::println);
		
		//(dropWhile((int a)->{a=1;}));
	}

}
