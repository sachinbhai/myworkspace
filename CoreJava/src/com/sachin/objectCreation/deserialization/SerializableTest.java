package com.sachin.objectCreation.deserialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.sachin.objectCreation.cloning.Address;

public class SerializableTest {
	public static void main(String[] args) {
		
		Patient p = new Patient();
		
		p.setId(1);
		p.setName("slokam");
		p.setEmail("slokam@gmail.com");
		p.setPhone(892637658L);
		
		Address address = new Address();
		
		address.setId(2);
		address.setCity("Hyderabad");
		address.setPincode("500013");
		address.setStreet("Gowlidoddi");
		
		p.setAddress(address);
		
		try {
			FileOutputStream fos = new FileOutputStream("D:\\obj.ser");
			ObjectOutputStream oob = new ObjectOutputStream(fos);
			oob.writeObject(p);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
