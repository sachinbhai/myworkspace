package com.sachin.objectCreation.deserialization;

import java.io.Serializable;

import com.sachin.objectCreation.cloning.Address;

public class Patient implements Serializable{
	
		/*Static, Transient declared variables and 
		native objects like FileInputStream will not be serialized*/
	
		private static final Long serialVersionUID=1L;
	
		private Integer id;
		private String name;
		private String email;
		private Long phone;
		
		private Address address;
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Long getPhone() {
			return phone;
		}
		public void setPhone(Long phone) {
			this.phone = phone;
		}
		public Address getAddress() {
			return address;
		}
		public void setAddress(Address address) {
			this.address = address;
		}
		
		
}