package com.sachin.objectCreation.deserialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationTest {
	
	public static void main(String[] args) {
		
		FileInputStream fis;
		try {
			fis = new FileInputStream("D:\\obj.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Patient p = (Patient) ois.readObject();
			
			System.out.println(p.getEmail());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		
	}

}
