package com.sachin.objectCreation.cloning;

public class CloningTest {
	public static void main(String[] args) {
		
		try {
			Policy p = new Policy();
			p.setId(1);
			p.setName("sachin");
			Address a = new Address();
			a.setId(1);
			a.setStreet("Osman Shahi");
			a.setPincode("500012");
			a.setCity("Hyderabad");
			
			p.setAddress(a);
			
			Policy p2 = p.clone();
			
			System.out.println(p2 == p);
			System.out.println(p2.equals(p));
			System.out.println(p2.getAddress() == p.getAddress());
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
	}
}
