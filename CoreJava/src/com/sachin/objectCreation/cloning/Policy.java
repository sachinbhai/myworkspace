package com.sachin.objectCreation.cloning;

public class Policy implements Cloneable {
	
	@Override
	public Policy clone() throws CloneNotSupportedException {
		Policy p = (Policy) super.clone();
		Address addr = p.getAddress().clone();
		p.setAddress(addr);
		return p;
	}
	
	private Integer id;
	private String name;
	//cloning an object with object, cloning address when policy is cloned = Deep Cloning
	//usually shallow cloning is done
	private Address address;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
}
