package com.sachin.objectCreation.cloning;

import java.io.Serializable;

public class Address implements Cloneable, Serializable {
	
	@Override
	public Address clone() throws CloneNotSupportedException {
		return (Address) super.clone();
	}
	
	private Integer id;
	private String street;
	private String city;
	private String pincode;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	
}
