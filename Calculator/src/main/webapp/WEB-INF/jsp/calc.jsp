<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>calc main page</title>

<script type="text/javascript">
	function addFun() {
		document.forms[0].action = "add";
		document.forms[0].submit();
	}
	function subFun() {
		document.forms[0].action = "sub";
		document.forms[0].submit();
	}
	function mulFun() {
		document.forms[0].action = "mul";
		document.forms[0].submit();
	}
	function divFun() {
		document.forms[0].action = "div";
		document.forms[0].submit();
	}
</script>

</head>
<body>

	<form>
		<fieldset>
			<legend>Calculator</legend>

			Enter a Value:<input type="number" name="val1"><br>
			Enter a Value:<input type="number" name="val2"><br> <input
				type="button" value="ADD" onclick="addFun()"> <input
				type="button" value="SUB" onclick="subFun()"> <input
				type="button" value="MUL" onclick="mulFun()"> <input
				type="button" value="DIV" onclick="divFun()">

		</fieldset>
	</form>

</body>
</html>