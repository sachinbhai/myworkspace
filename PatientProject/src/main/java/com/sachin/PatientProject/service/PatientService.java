package com.sachin.PatientProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sachin.PatientProject.entity.Patient;
import com.sachin.PatientProject.repos.PatientRepo;

@Service
public class PatientService {
	
	@Autowired
	private PatientRepo pRepo;
	
	public void saveMethod(Patient patient) {
		pRepo.save(patient);
	}
	
	public void saveAll(List<Patient> patientList) {
		pRepo.saveAll(patientList);
	}
	
}
