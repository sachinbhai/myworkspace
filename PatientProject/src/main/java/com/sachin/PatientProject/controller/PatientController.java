package com.sachin.PatientProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.PatientProject.entity.Appointment;
import com.sachin.PatientProject.entity.Doctor;
import com.sachin.PatientProject.entity.Patient;
import com.sachin.PatientProject.factoryUtil.AbstractFactory;
import com.sachin.PatientProject.factoryUtil.appointment.IAppointmentDao;
import com.sachin.PatientProject.factoryUtil.doctor.IDoctorDao;
import com.sachin.PatientProject.factoryUtil.patient.IPatientDao;
import com.sachin.PatientProject.service.PatientService;
import com.sachin.PatientProject.util.PatientDataUpload;
import com.sachin.PatientProject.util.RespMediator;
import com.sachin.PatientProject.util.RespPojo;
import com.sachin.PatientProject.util.IUploaderInterface;

@Controller
@RequestMapping("patient")
public class PatientController {
	
	Logger log = LoggerFactory.getLogger(PatientController.class);
	
	@Autowired
	public PatientService pService;
	
	@Autowired
	public RespMediator rMediator;
	
	@Autowired
	public RespPojo rPojo;
	
	@RequestMapping("/home")
	public ModelAndView pHome() {
		return new ModelAndView("patientHome", "patientAtt", new Patient());
	}
	
	@RequestMapping("/save")
	public ModelAndView pSave(@ModelAttribute("patientAtt") @Valid Patient patient, BindingResult br) {
		if(!br.hasErrors()) {
			pService.saveMethod(patient);	
		}else {
			return new ModelAndView("patientHome", "patientAtt", patient);
		}
		return new ModelAndView("patientHome", "patientAtt", new Patient());
	}
	
	@RequestMapping("/uploadFile")
	public ModelAndView pUpload(MultipartFile multipart) throws Exception {
		rPojo.setMultipart(multipart);
		IUploaderInterface ui = rMediator.getFirstResp();
		ui.performTask(rPojo);
		
		List<Patient> patientList = rPojo.getPatientList();
		log.info("before save");
		System.out.println(patientList.size()+"in controller");
		pService.saveAll(patientList);
		log.info("after save");
		
		return new ModelAndView("patientHome", "patientAtt", new Patient());
	}

}
