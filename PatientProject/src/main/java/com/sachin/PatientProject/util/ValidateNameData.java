package com.sachin.PatientProject.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.sachin.PatientProject.entity.Patient;

@Component
public class ValidateNameData extends IUploaderInterface{
	Logger log = LoggerFactory.getLogger(ValidateNameData.class);
	@Override
	public void performTask(RespPojo rPojo) throws Exception, IOException {
		log.info("start of Name validation");
		List<Patient> patientList = rPojo.getPatientList();
		
		Iterator<Patient> patients = patientList.iterator();
		
		while(patients.hasNext()) {
			Patient p = patients.next();
			if((p.getName().length())<5) {
				patients.remove();
			}
		}
		/*patients.forEachRemaining(p -> {
		if(p.getPhone()<1000000000 || p.getPhone()>9999999999L) {
			patients.remove();
		}
		} );*/
		log.info("end of Name validation");
		rPojo.setPatientList(patientList);
		System.out.println(patientList.size()+"in validName");
		gotoNextResp(rPojo);
	}
}
