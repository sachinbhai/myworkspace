package com.sachin.PatientProject.util2;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Component;

import com.sachin.PatientProject.entity.Patient;

@Component
public class XMLFileReading implements IReadingStrategyInterface{
	
	@Override
	public List<Patient> getStartegy(File file) throws Exception {
		List<Patient> patientList = new ArrayList<>();
		//String filePath = file.getPath();
		JAXBContext jxml = JAXBContext.newInstance(Patient.class);
		Unmarshaller unmarsh = jxml.createUnmarshaller();
		patientList = (List<Patient>) unmarsh.unmarshal(file);
		return patientList;
	
	}
	
}
