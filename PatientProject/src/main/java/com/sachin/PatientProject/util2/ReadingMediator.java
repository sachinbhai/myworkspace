package com.sachin.PatientProject.util2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReadingMediator {
	
	public static IReadingStrategyInterface chosenStrategy(String fileType) throws Exception {
		
		IReadingStrategyInterface strategy = null;
		
		switch (fileType) {
		case ".txt": strategy = new TextFileReading();	
			break;
		case ".xml": strategy = new XMLFileReading();
			break;
		case ".xlsx": strategy = new ExcellFileReading();
			break;
		case ".xls": strategy = new ExcellFileReading();
			break;
		default : throw new Exception();
		}
		
		return strategy;
	}
	
	/*@Autowired
	public TextFileReading tFileReading;
	
	@Autowired
	public XMLFileReading xFileReading;
	
	@Autowired
	public ExcellFileReading eFileReading;
	*/
}
