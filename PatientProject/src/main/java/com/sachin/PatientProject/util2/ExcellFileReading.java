package com.sachin.PatientProject.util2;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.sachin.PatientProject.entity.Patient;

@Component
public class ExcellFileReading implements IReadingStrategyInterface{
	
	@Override
	public List<Patient> getStartegy(File file) throws Exception {
		
		List<Patient> patientList = new ArrayList<>();
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheetAt(0);
		
		for(int i=0;i<=sheet.getLastRowNum(); i++) {
			XSSFRow row = sheet.getRow(i);
			DataFormatter formatter = new DataFormatter();
			XSSFCell cell0 = row.getCell(0);
			String name = cell0.getStringCellValue();
			XSSFCell cell1 = row.getCell(1);
			Long phone = Long.parseLong(formatter.formatCellValue(cell1));
			XSSFCell cell2 = row.getCell(2);
			String aadharCard = cell2.getStringCellValue();
			XSSFCell cell3 = row.getCell(3);
			Date dob = cell3.getDateCellValue();
			
			Patient patient = new Patient();
			patient.setAadharCard(aadharCard);
			patient.setDob(dob);
			patient.setName(name);
			patient.setPhone(phone);
			
			patientList.add(patient);
		}
		
		return patientList;
	}
}
