package com.sachin.PatientProject.util2;

import java.io.File;
import java.util.List;

import com.sachin.PatientProject.entity.Patient;

public interface IReadingStrategyInterface {

	public List<Patient> getStartegy(File file) throws Exception;
	
}
