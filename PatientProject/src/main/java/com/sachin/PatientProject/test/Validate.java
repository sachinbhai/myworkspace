package com.sachin.PatientProject.test;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.sachin.PatientProject.entity.Patient;

public class Validate {
	
	public void performTask(List<Patient> patientList) throws Exception, IOException {
		/*log.info("start of validation");
		List<Patient> patientList = rPojo.getPatientList();*/
		//patientList = rPojo.getPatientList();
		
		Iterator<Patient> patients = patientList.iterator();
		
		while(patients.hasNext()) {
			Patient p = patients.next();
			if((p.getName().length())<5) {
				patients.remove();
			}
		}
		
	}
}
