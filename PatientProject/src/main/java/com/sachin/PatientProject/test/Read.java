package com.sachin.PatientProject.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.sachin.PatientProject.entity.Patient;

public class Read {
	List<Patient> patientList = new ArrayList<>();
	public List<Patient> performTask(File file) throws Exception, IOException {
		/*log.info("start of readUpdatedFile task");
		File file = rPojo.getFile();*/
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheetAt(0);
		
		int lastRowNumber = sheet.getLastRowNum();
		for(int i=0; i<=lastRowNumber; i++) {
			DataFormatter formatter = new DataFormatter();
			XSSFRow row = sheet.getRow(i);
			XSSFCell cell0 = row.getCell(0);
			String name = formatter.formatCellValue(cell0);
			XSSFCell cell1 = row.getCell(1);
			Long phone = Long.parseLong(formatter.formatCellValue(cell1)); 
			XSSFCell cell2 = row.getCell(2);
			String aadharCard = formatter.formatCellValue(cell2);
			XSSFCell cell3 = row.getCell(3);
			Date dob = cell3.getDateCellValue();
			
			Patient patient = new Patient();
			patient.setDob(dob);
			patient.setAadharCard(aadharCard);
			patient.setName(name);
			patient.setPhone(phone);
			
			patientList.add(patient);
			//System.out.println(patient.getAadharCard());
		}
		
		/*for(Patient x: patientList) {
			System.out.println(x.getName());
		}*/
		/*rPojo.setPatientList(patientList);
		log.info("end of readUpdatedFile task");
		validateFileData.performTask(rPojo);
*/
	return patientList;	
	}
}
