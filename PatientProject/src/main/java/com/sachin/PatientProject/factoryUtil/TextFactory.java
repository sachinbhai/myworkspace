package com.sachin.PatientProject.factoryUtil;

import com.sachin.PatientProject.factoryUtil.appointment.AppointmentTextDao;
import com.sachin.PatientProject.factoryUtil.appointment.IAppointmentDao;
import com.sachin.PatientProject.factoryUtil.doctor.DoctorTextDao;
import com.sachin.PatientProject.factoryUtil.doctor.IDoctorDao;
import com.sachin.PatientProject.factoryUtil.patient.IPatientDao;
import com.sachin.PatientProject.factoryUtil.patient.PatientTextDao;

public class TextFactory extends AbstractFactory{
	
	@Override
	public IPatientDao getPatientDao() {
		return new PatientTextDao();
	}
	
	@Override
	public IDoctorDao getDoctorDao() {
		return new DoctorTextDao();
	}
	
	@Override
	public IAppointmentDao getAppointmentDao() {
		return new AppointmentTextDao();
	}

}
