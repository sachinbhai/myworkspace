package com.sachin.PatientProject.factoryUtil.patient;

import com.sachin.PatientProject.entity.Patient;

public interface IPatientDao {
	public void savePatient(Patient patient);
}
