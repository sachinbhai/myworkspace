package com.sachin.PatientProject.factoryUtil;

import com.sachin.PatientProject.factoryUtil.appointment.IAppointmentDao;
import com.sachin.PatientProject.factoryUtil.doctor.IDoctorDao;
import com.sachin.PatientProject.factoryUtil.patient.IPatientDao;

public abstract class AbstractFactory {
	
	public static AbstractFactory getFactory(String fileType) throws Exception {
		
		AbstractFactory af = null;
		switch (fileType) {
			case "excell": 	af = new ExcellFactory(); 	break;
			case "xml":	af = new XMLFactory();		break;
			case "text":	af = new TextFactory(); 	break;
			default: throw new Exception();
		}
		
		return af;
	}
	
	public abstract IPatientDao getPatientDao();
	public abstract IDoctorDao getDoctorDao();
	public abstract IAppointmentDao getAppointmentDao();
}
