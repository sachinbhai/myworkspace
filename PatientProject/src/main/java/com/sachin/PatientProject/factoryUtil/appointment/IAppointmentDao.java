package com.sachin.PatientProject.factoryUtil.appointment;

import com.sachin.PatientProject.entity.Appointment;

public interface IAppointmentDao {	
	public void saveAppointment(Appointment appt);
}
