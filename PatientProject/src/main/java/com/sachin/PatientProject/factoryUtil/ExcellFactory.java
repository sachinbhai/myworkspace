package com.sachin.PatientProject.factoryUtil;

import com.sachin.PatientProject.factoryUtil.appointment.AppointmentExcellDao;
import com.sachin.PatientProject.factoryUtil.appointment.IAppointmentDao;
import com.sachin.PatientProject.factoryUtil.doctor.DoctorExcellDao;
import com.sachin.PatientProject.factoryUtil.doctor.IDoctorDao;
import com.sachin.PatientProject.factoryUtil.patient.IPatientDao;
import com.sachin.PatientProject.factoryUtil.patient.PatientExcellDao;

public class ExcellFactory extends AbstractFactory {
	
	@Override
	public IPatientDao getPatientDao() {	
		return new PatientExcellDao();
	}
	
	@Override
	public IDoctorDao getDoctorDao() {
		return new DoctorExcellDao();
	}
	
	@Override
	public IAppointmentDao getAppointmentDao() {
		return new AppointmentExcellDao();
	}
	
}
