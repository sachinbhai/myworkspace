package com.sachin.PatientProject.factoryUtil.doctor;

import com.sachin.PatientProject.entity.Doctor;
import com.sachin.PatientProject.factoryUtil.ExcellFactory;

public class DoctorExcellDao implements IDoctorDao {
	@Override
	public void saveDoctor(Doctor doctor) {
		System.out.println("save doctor object in excell");
	}
}
