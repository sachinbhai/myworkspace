package com.sachin.MyFirstWebApp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.stereotype.Service;

@Service
public class LoginService {
	 
	public boolean validateUser(String userid, String password) {
		boolean isValid = false;	
		try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hibernate", "root", "root");
				PreparedStatement ps = con.prepareStatement("select * from user where userName=? and password=?");
				ps.setString(1, userid);
				ps.setString(2, password);
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					isValid=true;
				}
				rs.close();
				ps.close();
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return isValid;
	    }

}
