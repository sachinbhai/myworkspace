package com.sachin.RestApp2;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RestApp2Controller {
	
	@GetMapping("/restApp2Test")
	public String runApp2(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> re = restTemplate.getForEntity("http://localhost:2010/restApp1Test", String.class);
		String str = re.getBody();
		return "restApp2Test"+str;
	}
}
