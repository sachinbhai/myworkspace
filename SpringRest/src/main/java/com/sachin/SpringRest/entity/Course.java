package com.sachin.SpringRest.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class Course {
	
	@Id
	@GeneratedValue
	private Integer id;
	private String courseName;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
		name ="student_course", 
		joinColumns = { @JoinColumn(name ="cid")},
		inverseJoinColumns = {@JoinColumn(name="sid")}
	)
	private List<Student> stuCourse;
	
	public List<Student> getStuCourse() {
		return stuCourse;
	}
	public void setStuCourse(List<Student> stuCourse) {
		this.stuCourse = stuCourse;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	
	
}
