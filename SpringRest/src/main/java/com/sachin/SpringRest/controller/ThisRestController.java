package com.sachin.SpringRest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.SpringRest.entity.Address;
import com.sachin.SpringRest.entity.Course;
import com.sachin.SpringRest.entity.Passport;
import com.sachin.SpringRest.entity.Student;

@RestController
public class ThisRestController {

	@RequestMapping("test1")
	public ResponseEntity<String[]> test1() {
		String[] x = {"sachin ", "is ", "doing ", "fine."};
		return new ResponseEntity<>(x, HttpStatus.OK);
	}
	
	@RequestMapping("test2")
	public Map<Integer, String> test2() {
		String[] y = {"sachin ", "is ", "doing ", "fine."};
		
		Map<Integer, String> x = new HashMap<>();
		
		x.put(1, y[0]);
		x.put(2, y[1]);
		x.put(4, y[3]);
		x.put(3, y[2]);
		
		return x;
	}
	
	@RequestMapping("test3")
	public Map<Integer, Student> test3() {

		Map<Integer, Student> x = new HashMap<>();
		List<Student> studentList = new ArrayList<>();
		
		Student stu = new Student();
		stu.setId(1);
		stu.setName("sachin");
		studentList.add(stu);
		
		Student stu2 = new Student();
		stu2.setId(2);
		stu2.setName("harish");
		studentList.add(stu2);
		
		Student stu3 = new Student();
		stu3.setId(3);
		stu3.setName("ravi");
		studentList.add(stu3);
		
		Student stu4 = new Student();
		stu4.setId(4);
		stu4.setName("satya");
		studentList.add(stu4);
		
		x.put(1, studentList.get(0));
		x.put(2, studentList.get(1));
		x.put(4, studentList.get(3));
		x.put(3, studentList.get(2));
		
		return x;
	}
	
	@RequestMapping("test4")
	public List<Student> test4() {
		
		List<Student> studentList = new ArrayList<>();
		
		Student stu = new Student();
		stu.setId(1);
		stu.setName("sachin");
		studentList.add(stu);
		
		Student stu2 = new Student();
		stu2.setId(2);
		stu2.setName("harish");
		studentList.add(stu2);
		
		Student stu3 = new Student();
		stu3.setId(3);
		stu3.setName("ravi");
		studentList.add(stu3);
		
		Student stu4 = new Student();
		stu4.setId(4);
		stu4.setName("satya");
		studentList.add(stu4);
		
		return studentList;
	}
	
	@RequestMapping("test5")
	public Student test5() {
		
		Passport passport = new Passport();
		Student stu = new Student();

		stu.setId(1);
		stu.setName("sachin");
		/*studentList.add(stu);
		stu.setId(2);
		stu.setName("harish");
		studentList.add(stu);
		stu.setId(3);
		stu.setName("ravi");
		studentList.add(stu);
		stu.setId(4);
		stu.setName("satya");
		studentList.add(stu);*/
		passport.setId(1);
		passport.setPassportNo("HR0123");
		//Date d1 = new Date(23, 12, 2019);
		passport.setExpDate(new Date());
		
		stu.setPassport(passport);
		
		return stu;
	}
	
	@RequestMapping("test6")
	public Student test6() {
		
		Student stu = new Student();
		Passport p = new Passport();

		stu.setId(1);
		stu.setName("sachin");
		/*studentList.add(stu);
		stu.setId(2);
		stu.setName("harish");
		studentList.add(stu);
		stu.setId(3);
		stu.setName("ravi");
		studentList.add(stu);
		stu.setId(4);
		stu.setName("satya");
		studentList.add(stu);*/
		p.setId(1);
		p.setPassportNo("HR0123");
	
		stu.setPassport(p);
		
		List<Address> addr = new ArrayList<>();
		
		Address a = new Address();
		a.setId(1);
		a.setArea("hyderabad");
		a.setPincode(500012L);
		addr.add(a);
		
		Address a1 = new Address();
		a1.setId(2);
		a1.setArea("secunderabad");
		a1.setPincode(500054L);
		addr.add(a1);
		
		stu.setAddr(addr);
		
		return stu;
	}
	
	@RequestMapping("test7")
	public Student test7() {	
		
		Student stu = new Student();
		Passport p = new Passport();

		stu.setId(1);
		stu.setName("sachin");
		
		p.setId(1);
		p.setPassportNo("HR0123");
		stu.setPassport(p);
		
		List<Address> addr = new ArrayList<>();
		
		Address a = new Address();
		a.setId(1);
		a.setArea("hyderabad");
		a.setPincode(500012L);
		addr.add(a);
		
		Address a1 = new Address();
		a1.setId(2);
		a1.setArea("secunderabad");
		a1.setPincode(500054L);
		addr.add(a1);
		
		stu.setAddr(addr);
		
		List<Course> courses = new ArrayList<>();
		Course c = new Course();
		c.setId(1);
		c.setCourseName("Java");
		courses.add(c);
		
		Course c2 = new Course();
		c2.setId(2);
		c2.setCourseName("JavaScript");
		courses.add(c2);
		
		Course c3 = new Course();
		c3.setId(3);
		c3.setCourseName("Python");
		courses.add(c3);
		
		stu.setCourses(courses);

		return stu;
	}

}
