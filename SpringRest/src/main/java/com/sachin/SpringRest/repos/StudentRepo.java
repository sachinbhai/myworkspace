package com.sachin.SpringRest.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.SpringRest.entity.Student;

@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {

}
